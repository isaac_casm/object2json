var object2json = {

    // Inheritance Fruit.prototype = new Plant ();
    obj2json: function(obj, not_save_keys=[]){
        //Save an object into JSON by recursively taking obj, arrays, strings, etc...
        //:param obj: The object
        var result = {};
        if(typeof obj == 'Array')
            result = [];
        for (var i in obj){
            var t, ex;

            try{
                t = typeof obj[i];
            }
            catch (ex){
                t = "error";
            }
            if(not_save_keys.indexOf(i) > -1) continue;

            var string_object = '';
            switch (t)
            {
                case "function" || "error":
                    continue
                    break;

                case "object" || "Array":
                    string_object = this.obj2json(obj[i], not_save_keys);
                    break;

                default:
                    //string_object = JSON.stringify(obj[i]);
                    string_object = JSON.stringify({'constructor':'standard' ,'values':JSON.stringify(obj[i])});
                    break;

            }
            if(typeof object == 'Array')
                result.push(string_object);
            else
                result[i] = string_object;
        }

        return JSON.stringify({'constructor':obj.constructor.name ,'values':result});
    },
    
    instanciateObject: function(classNameString){
    	var global;
		try {
		  global = Function('return this')() || (42, eval)('this');
		} catch(e) {
		  global = window;
		}

        // and then
        var a = new global[classNameString];
		return new global[classNameString]
    },

    json2obj: function(jsonVar, constructors){
        //Load the objects
        //:param jsonVar: A json string with all the information to load.
        //:param constructors: Dictionary with all the constructors that can be used.
        
        var dict = JSON.parse(jsonVar);

        if(dict['constructor'] in constructors){
            var obj = new constructors[dict['constructor']]()
        }
        else{
            var obj = this.instanciateObject(dict['constructor']) //{};
        }
            
        for(key in dict['values']){
            var current_value = JSON.parse(dict['values'][key]);
        	
            if (current_value['constructor'] == 'standard')
                current_value = JSON.parse(current_value['values']);       		
        	else
                current_value = this.json2obj(dict['values'][key],constructors);
				
        	if(dict['constructor'] == 'Array')
        		obj.push(current_value)
        	else{
        		if(key in obj)
            		obj[key] = current_value;
            }
        }
        return obj
    }
}
module.exports = object2json;