var object2json = require('../src/object2json.js');

var class_base = function(id){
    this.one_func = function(){return 1;}

    this.variable = 2;
    this.id = id;
}
var another_class = function(){
    this.var = 1;
    this.array = [new class_base(0), new class_base(1)];
    

    this.dict = {'2':new class_base(2), '3':new class_base(3)};

    this.dict_dict = {'one':{'4':new class_base(4)}};
}
var test_obj2json = function(){
    var example = new another_class();
    var json_obj = object2json.obj2json(example)

    console.log(json_obj)
}

var test_json2obj = function(){
    var example = new another_class();
    var json_obj = object2json.obj2json(example)
    console.log(json_obj)

    var dict = {'class_base':class_base, 'another_class':another_class}
    var obj = object2json.json2obj(json_obj, dict);

    console.log(json_obj)
}

//test_obj2json();
test_json2obj();